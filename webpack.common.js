/* eslint-disable */
const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const copyWebpackPlugin = require('copy-webpack-plugin');
const cleanWebpackPlugin = require('clean-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssPresentEnv = require('postcss-preset-env');
const marked = require('marked');
const fs = require('fs');

const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  plugins: [
    new cleanWebpackPlugin(['dist']),
    new htmlWebpackPlugin({
      title:'Hello there!',
      template:'./src/index.html'
    }),
    new copyWebpackPlugin([
      {
        from:'src/pages',
        to:'pages/[name].html',
        transform(content,path) {
          console.log('path',path);
          const mdContent = fs.readFileSync(path, 'utf8');
          return marked(mdContent);
        }
      } 
    ]),
    new StyleLintPlugin({
      configFile: '.stylelintrc',
      context: 'src',
      files: 'css/*.css',
      failOnError: false,
      quiet: false,
    }),
    new MiniCssExtractPlugin()
  ],
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        //test: /\.(js|jsx)$/,
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            plugins: ['transform-async-to-generator']
          }
        }
      },
      {
        test: /\.css$/,
        use:[
          {
            loader: 'style-loader'
          },
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          {
            loader: 'postcss-loader'
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader'
      }  
    ]
  }
};