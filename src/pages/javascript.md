# JavaScript

## 'this' keyword
Ask, where is this function invoked (not defined)?

### Implicit binding
Look to the left of the dot

### Explicit binding
Send the context to a function via the `call` method
`fun.call(obj,args1,argsN)`

`apply` accepts an array

`bind` returns new function which you can call later
`var newFun = someFun.bind(obj,args1,argsN)`

### new binding
When a object is created with the `new` keyword it inherits the base `object` too

### window binding
Unless using the above, the object defaults to the `window` object
Adding `'use strict';` will throw an error if trying to access a property from within a context of a function to the window object

## Handy Syntax

### if shorthand

if this bit is true **?** then return this **:** otherwise return this

```javascript
1 > 3 ? true : false;
```


### array.map
```javascript
{this.props.list.map(item => <li key={item.id}>{item.prop}</li>)}
```

### array.filter

```javascript
{this.props.list.filter(item => item.prop === true).map(item => <li key={item.id}>{item}</li>)}
```


```javascript
{this.props.list.filter(item => item.prop === true).map(item => <li key={item.id}>{item}</li>)}
```


### array.find
```javascript
{const found = this.props.list.find(item => item.id === theIdImLookingFor)}
```

### reduce
reduce an array down to one value. Set the initial value `0`. Whatever gets returned gets passed in as the first argument. The second argument is the current item in the array.
```javascript
  return repos.data.reduce(function(count, repo){
    return count + repo.stargazers_count
  },0)
```

### Promise all
```javascript

Promise.all([
  apiInvoke1(),
  apiInvoke2(someArg)
]).then( ([apiReturn1, apiReturn2]) => {
  //then use the data, ie
  this.setState( () => {
    loading: false,
    apiReturn1,
    apiReturn2
  })
})

```

## Helpful libraries

### Body Scroll Lock
Enables body scroll locking (for iOS Mobile and Tablet, Android, desktop Safari/Chrome/Firefox) without breaking scrolling of a target element (eg. modal/lightbox/flyouts/nav-menus).
https://github.com/willmcpo/body-scroll-lock

### Shave
Truncate multi line text and adds ellipses
https://dollarshaveclub.github.io/shave/