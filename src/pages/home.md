# Front-end Development ToolKit

(https://bitbucket.org/mtoddoz/folio/src/master/)

### HTML
* Semantics
* Responsive
* Accessibility
* Progressive Web Apps (PWAs)
* UI Testing
* Menus

### CSS
* [PostCSS](/postcss)
* CSS Grids / Layout
* Stylelint (https://stylelint.io/)

### JavaScript
* ES6
* React
* Unit testing
* Animations
* Charts
* Service workers
* Websockets

### Build Systems
* NPM
* Yarn
* Webpack
* Gulp
* Grunt

### Hosting


### CDCI
* GIT