# React Router v4

### Need to pass props? Use render!
```javascript
<Route path="/stats" render={(props) => <Stats {...props} teamId="3" /> }>
```

### Redirects
There is a component for that!
or 
```javascript
import { withRouter } from 'react-router-dom';
...
this.props.history.push('/new-destination');
...

//ensures prop.history exists
export default withRouter(WhateverThisComponentIs);
```

### Querystrings
```javascript
import queryString from 'query-string';

// ?search=turbos

//raw
const queryString = this.props.location.search;

//browser API (check canuse)
const params = new URLSearchParams(queryString);
params.get('search');

//use QueryString package (good browser support)
const values = queryString.parse(queryString);
values.search //turbos
```

### Passing state between links
```javascript
<Link to={{
  pathname: '/tylermcginnis',
  state: {
    fromNotifications: true
  }
}}>Tyler McGinnis</Link>
```

### Protected / Auth Route
```javascript
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    authThingy.isAuthenticated === true
      ? <Component {...props} />
      : <Redirect to='/login' />
  )
)
```