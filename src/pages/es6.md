# ES6 Magic

## Async/Await
```javascript
const foo = async () => {
  const stuff = await somePromise();
}
```

## Destructuring
```javascript
//basic object
const { prop } = someObjectWithAProp;

//array
var someArray = ["green","male",6];
var [ colour, sex, size ] = someArray;

//from within function and rename a prop
const myArryOfObjs = [
  {
    name: 'James',
    age: 42
  },
  {
    name: 'Will',
    age:3
  }
];

myArryOfObjs.map(({ name: firstname ,age }) => {
  console.log(firstname,age);
});

//function definition with default values

function amazing({ prop1, prop2='something' }) {

}

amazing({
  'before something'
})


```

## Shorthand property name
If key matches the property value, you can exclude the key

```javascript
//name, age, location already defined
 const someObj = {
   name,
   age,
   location,
   sendLocation () {
     //some method
   }

 }
```

## Computed Property Names
```javascript

function returnMeAnObject(key,val) {
  return {
    [key]: val
  }
}

```

## Log with implicit return
```javascript
this.setState( () => console.log(someData) || ({theData: someData}) )
```

## Default parameters (with function!)
```javascript

function isRequired(name) {
  throw new Error(`${name} is required`);
}

function doLogin(userId = isRequired('name'), howLong = 20) {

}

```