# React.js

### Ultimate resource guide
(https://github.com/enaqx/awesome-react)

## Why react

Compoition of components

Imperative vs Declarative code
Imperative tell the computer how to do it, step by step
Declarative what we want to have happen

Unidirectional data flow

Explicit mutations
Have to call `setState`

Just Javascript

## Component Blueprint
```javascript
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class MyComp extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    featureA: 'this feature a on the state'
  }

  //ajax calls here
  componentDidMount() {

  }

  //further changes to component already mounted
  componentDidUpdate() {

  }

  //must have this
  render() {
    const { featureA } = this.state;
    const { propA } = this.props;

    return (<div>{featureA} {propA}</div>);
  }

}

MyComp.defaultProps = {
  propA: 'The default prop state'
}

MyComp.propTypes = {
  propA: PropTypes.string.isRequired
}

export default MyComp;

```


## Live cycle methods
(https://daveceddia.com/where-fetch-data-componentwillmount-vs-componentdidmount/)

### initial state
```javascript
this.state = {
  name: 'You have no name'
}
```

### default props
```javascript
MyComp.defaultProps = {
  name: 'You still have no name yet'
}
```

### componentDidMount
ajax requests and websockets

### componentDidUpdate
if component received new props, check if they have changed to make updates to component

### componentWillUnmount
remove listeners

### getDerivedStateFromProps
The object you return from this function will be merged with the current state
`getDerivedStateFromProps(nextProps, prevState)`

## PropTypes
(https://reactjs.org/docs/typechecking-with-proptypes.html)

`npm i prop-types -D`

```javascript
import PropTypes from 'prop-types';

 MyComponent.propTypes = {
   name: PropTypes.string.isRequired,
   age: PropTypes.number,
   interests: PropTypes.arrayOf(PropTypes.shape({
     type: PropTypes.string.isRequired,
     value: PropTypes.number.isRequired
   }))
 }

 MyComponent.defaultProps = {
   name: 'James Bond',
   age: 42
 }
```

## Handy Snippets

### if prop exists return jsx
```javascript
{something.isHere && <p>something.isHere</p>}
{!something.isNotHere && <p>something.isNotHere</p>}
{something.isHere && something.elseIsHere && <p>something.isHere</p>}
```

### children props
```javascript
const MyComp = (props) => <div><h1>Cool thing</h1>{props.children}</div>
...
<MyComp>this way!</MyComp>
```

## Mocking APIs
(https://github.com/typicode/json-server)
(https://medium.com/@muratcorlu/mocking-rest-endpoints-in-web-apps-easy-way-d4cd0e9db000)


## Animations
Transition Group
CSS Transitition

## Code splitting
Two methods;
* Split a route level
* Split at component level

```javascript
if (editPost === true) {
  import('./editpost')
    .then((module) => module.showEditor())
    .catch((e) => )
}
```

React loadable

## Server Side Rendering (SSR)
Instead of `render()` from 'react-dom' use `hydrate()`
(https://github.com/tylermcginnis/rrssr)
 