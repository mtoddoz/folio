# Forms

### Validation
- HTML5 forms have build in validation and pseudo css flags
- Use javascript to override default behavior and add customisations

More info
https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/Form_validation
