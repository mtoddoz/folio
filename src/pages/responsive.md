set meta tag viewport to ensure 1:1 pixel ratio for device
```html
  <meta name="viewport" content="width=device-width, initial-scale=1">
```

use media queries to target different display sizes (phone, tablet, desktop, tv
```html
  <link rel="stylesheet" media="(max-width: 640px)" href="max-640px.css">
  <link rel="stylesheet" media="(min-width: 640px)" href="min-640px.css">

  @media (query) {
    //min-width
    //max-width
  }

  //don't use min-device-width (bad legacy browser support)
```

##SVGs
https://svgontheweb.com/