# CSS

### Lightning history of css.
* Basic style.css stylesheets
* Introduction of preprocessors, SASS, Less, Stylus
* Post processors, PostCss

### Layouts
* Floats (past)
* Flexbox (present)
* CSS Grid (future) (https://css-tricks.com/snippets/css/complete-guide-grid/)

### Methodologies
* OOCSS (Object Oriented CSS) 2009, sepatate structure and skin
* SMACSS (Scalable and Modular Architecture for CSS) 2011, base rules, layout rules, modules, state rules, and theme rules
* BEM (Block, Element, Modifier) 2010
* Atomic or Functional CSS 2014 `<button class="f6 br3 ph3 pv2 white bg-purple hover-bg-light-purple">Search</button>` Tachyons, Tailwind
* CSSin JS, 2014, components with hard boundaries
* Styled Components

## Media Query
```css
@media only screen and (max-width: 600px) {
  //specifc style here
}
```

## CSSNext
(http://cssnext.io)

## PostCSS

Abstraction layer for css with a huge amount of plugins and APIs

(https://postcss.org)


## Selectors

## Box Model
Box sizing

## Sizing units
px
em
rem
vh

## Pseudo elements
