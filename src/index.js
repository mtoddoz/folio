import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, NavLink, Switch } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

//components
import Menu from './components/menu';
import Page from './components/page';
import Breadcrumb from './components/breadcrumb';
import Loading from './components/loading';

//styles
import './css/style.css';

class App extends React.Component {

  constructor(props){
    super(props)

    this.state = {
      content: ''
    }
  }

  render() {
    return (

      <Router>
        <Route render={({ location }) => (

        <div className="mw9 center ph3-ns">
          <div className="cf ph2-ns">
            <div className="fl w-100 w-20-ns pa2">
              <div className="bg-white">
                <Menu />
              </div>
            </div>
            <div className="fl w-100 w-80-ns pa2">
              <div className="bg-white pa2 mw-4">
                <TransitionGroup>
                <CSSTransition 
                  key={location.key}
                  classNames="fade"
                  timeout={300}
                >
                  <Switch location={location}>
                    <Route path="/" exact render={() => <Page page='home'/>} />
                    <Route path="/:page" component={Page} />
                    <Route render={() => <p>not found</p>} />
                  </Switch>
                </CSSTransition>
              </TransitionGroup>
              <Route path="/" render={(props) => (
                <Breadcrumb id='that was pointless' {...props}>
                  {
                    (id) => <div>the magic id {id}</div>
                  }
                </Breadcrumb>) } />
              </div>
            </div>
          </div>
        </div>
          

          
        )} />
        
      </Router>
    )
  }
}


const rootElm = document.getElementById('app_root');
ReactDOM.render(<App/>, rootElm);