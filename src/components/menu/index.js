import React from 'react';
import { NavLink } from 'react-router-dom';

// import HamburgerIcon from '../../static/svg/hamburger.svg';


class Menu extends React.Component {
  state = {
    menuItems: [
      'css',
      'es6',
      'javascript',
      'react',
      'react-router',
      'responsive',
      'styleguide',
      'webpack',
    ],
    navClassNames: 'absolute relative-ns bg-white w-50 w-20-ns h-100 dn db-ns',
    menuOpen: false,
    menuText: 'Open',
  }

  toggleMenu = () => {
    const { menuOpen, navClassNames } = this.state;

    this.setState(() => {
      return {
        navClassNames: menuOpen ? 'absolute relative-ns bg-white w-50 w-20-ns h-100 dn db-ns' : 'absolute relative-ns bg-white w-50 w-100-ns h-100 db-ns',
        menuOpen: !menuOpen,
        menuText: menuOpen ? 'Open' : 'Close',
      };
    });
  }

  render() {
    const { menuItems, navClassNames, menuText } = this.state;
    return (
      <div>
        <button className="dn-ns" onClick={this.toggleMenu}>{menuText}</button>
        <nav className={navClassNames} >  
          <ul className="list pa0">
            <li className="pa2"><NavLink to="/">home</NavLink></li>
            {menuItems.sort().map( (item,index) => (
              <li 
                key={index} 
                className="pa2">
                <NavLink 
                  to={item} 
                  activeClassName="bold"
                  onClick={this.toggleMenu}
                >
                  {item}
                </NavLink>
              </li>
            ))}
          </ul>
        </nav>
      </div>
    )
  }
}

Menu.propTypes = {

};

Menu.defaultProps = {

};

export default Menu;