import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import Menu from './index';


describe('<Menu />', () => {
  test('Should render correctly', () => {
    const component = renderer.create(<Menu />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  test('Should click a menu item', () => {
    const onClick = jest.fn();

    const component = mount(<Menu onClick={onClick} />);

    component.simulate('click');

    expect(onClick.mock.calls.length).toBe(1);

  })
})