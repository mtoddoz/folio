import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class Breadcrumb extends Component {
  static propTypes = {
    location: PropTypes.shape({
      pathname: PropTypes.string,
    }),
  }

  // ajax calls here
  componentDidMount() {

  }

  // further changes to component already mounted
  componentDidUpdate() {

  }

  // must have this
  render() {
    const { location: { pathname } } = this.props;

    return (
      <div>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to={pathname}>{pathname}</Link></li>
        </ul>
      </div>
    );
  }
}

Breadcrumb.defaultProps = {
  location: {
    pathname: '/',
  },
};

export default Breadcrumb;
