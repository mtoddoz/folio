//core
import React from 'react';
import { Redirect } from 'react-router-dom';

//utils
import axios from 'axios';

//style

//components
//import Loading from '../loading';


class Page extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      content: null,
      loading: true
    }
  }

  componentDidMount() {
    this.getPageContent();
  }

  componentDidUpdate(prevProps) {
    if(prevProps.match.params.page !== this.props.match.params.page) {
      this.getPageContent();
    }
  }

  getPageContent() {
    
    let page;

    if(typeof this.props.match.params != 'undefined') {
      page = this.props.match.params.page
    } else if(typeof this.props.page != 'undefined') {
      page = this.props.page
    } else {
      page = '404';
    }
    
    axios.get(`/pages/${page}.html`)
    .then(res => res.data)
    .catch(error => this.props.history.push('/404'))
    .then(html => {
      this.setState({
        content: html,
        loading: false
      })
    });
  }

  render() {
   
    const content = {
      __html: this.state.content
    }

    const { loading } = this.state;

    if(loading) {
      return (<b>loading</b>)
    }

    return (
      <div className="mw9">
        <article className="main-article" dangerouslySetInnerHTML={content}></article>
      </div>
    )
      
    
  }
}

Page.defaultProps = {
  match: {
    params: {
      page: 'home'
    }
  }
}

export default Page;