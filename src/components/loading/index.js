import React from 'react';
import PropTypes from 'prop-types';

class Loading extends React.Component {
  static propTypes = {
    text: PropTypes.string,
    time: PropTypes.number,
  }

  static defaultProps = {
    text: 'Loading',
    time: 300,
  }

  state = {
    text: this.props.text,
  }

  componentDidMount() {
    const { text } = this.props;
    const { text: stateText } = this.state;
    const stopper = `${text}...`;

    this.interval = setInterval(() => {
      if (stateText === stopper) {
        this.setState(() => ({ text }));
      } else {
        this.setState(() => ({ text: text + '.' }));
      }
    }, this.props.time);
  }

  componentWillUnmount() {
    window.clearInterval(this.interval);
  }

  render() {
    return (
      <div>
        {this.state.text}
      </div>
    );
  }
}

export default Loading;
