import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import Loading from './index';


describe('<Loading />', () => {
  test('Should render correctly', () => {
    const component = renderer.create(<Loading />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  /*
  test('Should click a menu item', () => {
    const onClick = jest.fn();

    const component = mount(<Menu onClick={onClick} />);

    component.simulate('click');

    expect(onClick.mock.calls.length).toBe(1);

  })
  */
})