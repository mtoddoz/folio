module.exports = {
  plugins: {
    'postcss-import': {},
    'cssnano': {},
    'postcss-preset-env': {
      stage: 4,
      features: {
        'nesting-rules': true
      }
    },
    'postcss-custom-media': {},
    'postcss-custom-properties': {},
    'autoprefixer': {},
  }
}